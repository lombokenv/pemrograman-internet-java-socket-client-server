/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quizfinal;


import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Scanner;


public class Client implements Runnable{
    private Socket socket;
    private final int PORT = 1234;
    private String ipServer = "localhost";
    private boolean isRunning = false;
    
    private DataOutputStream outputStream;
    private ObjectInputStream inputStream;
    
    
    
    private Client(){
     
      try{
        System.out.println("Connecting to Server"+" on Port "+PORT);
        socket = new Socket(ipServer,PORT);
        
        System.out.println("Success! connected to "+ socket.getRemoteSocketAddress());
        
        inputStream = new ObjectInputStream(socket.getInputStream());
        outputStream = new DataOutputStream(socket.getOutputStream());
        
      }catch(Exception e){e.printStackTrace();}
    }
    
    public void sendRequestToServer(Object object) throws IOException{
        if (object instanceof Boolean)
            outputStream.writeBoolean((Boolean) object);
        if (object instanceof String)
            outputStream.writeUTF((String)object);
    }

//    private String getAnswer(Object obj){
//        String yes = "";
//        if("1".equals(obj)){
//            yes = "yes";
//        }
//        return yes;
//    }
    
    private boolean checkAnswers(String answer, String corectAnswer) throws IOException{
    
        boolean answersIsCorrect = false;
        if(answer.equals("stop")){
            sendRequestToServer("stop");
        }
        if(answer.equals(corectAnswer)){
            System.out.println("Your Answers is Correct!");
            answersIsCorrect = true;
        }else{
            System.out.println("Ups! Your Answers is inCorrect!");
            answersIsCorrect = false;
        }
        return answersIsCorrect;
    }
    @Override
    public void run() {
       
       try{
            isRunning = true;
            Scanner input = new Scanner(System.in);
            String answer = "";
            System.out.println(inputStream.readObject());
            sendRequestToServer(isRunning);
            
            while(isRunning){
                
                System.out.println("Silahkan ketik yes untuk memulai permainan");
                answer = input.nextLine();
                int i = 0;
                int Scores = 0;
                while(!answer.equals("stop")){
                    if(i == 5){
                        break;
                    }
                    sendRequestToServer(answer);
                    String questions = (String)inputStream.readObject();
                    String answers = (String)inputStream.readObject();
                    System.out.println(questions);
                    
                    
                    
                    answer = input.nextLine();
                    if(checkAnswers(answer, answers) == true){
                        Scores += 20;
                    }
                    i++;
                }
                System.out.println("Your Scores :"+Scores);
                isRunning = false;
            }
           
       }catch(Exception e){
           e.printStackTrace();
       }
    }
    
    public static void main(String[] args) {
        Client client = new Client();
        new Thread(client).start();
    }
    
}
