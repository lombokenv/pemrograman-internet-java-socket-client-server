
package quizfinal;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;



public class Server implements Runnable{
    
    private ServerSocket serverSocket;
    private final int PORT = 1234;
    private Socket socket;
    
    private boolean isRunning = false;

    private ArrayList<Quiz> quizList = new ArrayList<>();
    private ArrayList<Socket> listOfClients = new ArrayList<>();
   
   
    
    private void generateQuiz(){
        //Membuat objek quiz sebanyak 5        
        Quiz quiz1 = new Quiz(1,"Tekan 1 Untuk Mulai","1");
        Quiz quiz2 = new Quiz(2," = 4, 2 = 16, 3 = 64, 4 = ?","1");
        Quiz quiz3 = new Quiz(3,"Jika Kamu Air, Aku Adalah...","kamu");
        Quiz quiz4 = new Quiz(4,"Level Binary","0100");
        Quiz quiz5 = new Quiz(5,"sila","panca");
        
        //Menambah 5 quiz ke dalam quizList  
        quizList.add(quiz1);
        quizList.add(quiz2);
        quizList.add(quiz3);
        quizList.add(quiz4);
        quizList.add(quiz5);
        
    }
    
    private void initServer() throws IOException{
        System.out.println("Server waiting connections...");
        generateQuiz();
        serverSocket = new ServerSocket(1234);
    }
    
    private void play(Socket client){
        try(
            //Stream for client
            ObjectOutputStream outputStream = new ObjectOutputStream(client.getOutputStream());
            DataInputStream inputStream = new DataInputStream(client.getInputStream());
            ){    
            
            outputStream.writeObject("###Welcome to Quiz Parampa");
            while(inputStream.readBoolean()){
                int i = 0;
                while(!inputStream.readUTF().equals("stop")){
                    if(i == quizList.size()){
                        break;
                    }
                    sendQuiz(outputStream,i);
                    sendAnswer(outputStream,i);
                    i++;
                }
            }
        }catch(Exception e){e.printStackTrace();}
        
    }
    private void sendQuiz(ObjectOutputStream outputStream, int i) throws IOException {
//        outputStream.writeObject("("+quizList.get(i).getNumberQuiz()+")"+quizList.get(i).getQestionQuiz());
        outputStream.writeObject(quizList.get(i).getQestionQuiz());
    
    }
    private void sendAnswer(ObjectOutputStream outputStream, int i) throws IOException {
        outputStream.writeObject(quizList.get(i).getAnswerQuiz());
    }
    @Override
    public void run(){
        
        try{
            isRunning = true;
            initServer();
            while(isRunning){
                socket = serverSocket.accept();
                listOfClients.add(socket);
                System.out.println("New Client"+listOfClients.size()+" Connected!");
                Runnable r = () -> play(socket);
                new Thread(r).start();
            }
        }catch(Exception e){e.printStackTrace();}
    
    }
    
    public static void main(String[] args){
       new Server().run();
    }


}
