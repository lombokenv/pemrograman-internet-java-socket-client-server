/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quizfinal;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author juliet
 */
public class GamePlay extends javax.swing.JFrame implements Runnable,KeyListener{

    /**
     * Creates new form GamePlay
     */
    private ArrayList<String> questionList = new ArrayList<>();
    private Socket socket;
    private final int PORT = 1234;
    private String ipServer = "localhost";
    private boolean isRunning = false; //boolean untuk running program
    private boolean mouseInput = false; //boolean untuk input answer
    
    private int heart;
    private String answer;
    private int Scores;
    private DataOutputStream outputStream;
    private ObjectInputStream inputStream;
    
    private String answerMouse; //var untuk menyimpan answer
    private int currentLevel; //untuk mengecek level sekarang dipakai oleh Keyboard Level 1
    
    public GamePlay() {
        initComponents();
        setTitle("Game Play");
        setLocationRelativeTo(null);
        setResizable(false);
        addKeyListener(this);
        try{
            System.out.println("Connecting to Server"+" on Port "+PORT);
            socket = new Socket(ipServer,PORT);

            System.out.println("Success! connected to "+ socket.getRemoteSocketAddress());

            inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream = new DataOutputStream(socket.getOutputStream());

        }catch(Exception e){e.printStackTrace();}
      
    }
    
    public void setQuestion(String q){
         questionList.add(q);
    }
    
    
    /**
     * GAME LOGIC
     */
    
    public void sendRequestToServer(Object object) throws IOException{
        if (object instanceof Boolean)
            outputStream.writeBoolean((Boolean) object);
        if (object instanceof String)
            outputStream.writeUTF((String)object);
    }
    
    private boolean checkAnswers(String answer, String corectAnswer) throws IOException{
    
        boolean answersIsCorrect = false;
        if(answer.equals("stop")){
            sendRequestToServer("stop");
        }
        if(answer.equals(corectAnswer)){
            System.out.println("Your Answers is Correct!");
            answersIsCorrect = true;
        }else{
            System.out.println("Ups! Your Answers is inCorrect!");
            answersIsCorrect = false;
        }
        return answersIsCorrect;
    }
    
    @Override
    public void run() {
       
       try{
            isRunning = true;
            
            Scanner input = new Scanner(System.in);
            answer = "";
            System.out.println(inputStream.readObject());
            sendRequestToServer(isRunning);
            while(isRunning){

                answer = "mulai";
                int i = 0;
                currentLevel = 0;
                Scores = 0;
                heart = 3;
                
                while(!answer.equals("stop") || heart < 0){
                    System.out.println(heart);
                    if(i == 5){
                        break;
                    }
                    
                    sendRequestToServer(answer);
                    String questions = (String)inputStream.readObject();
                    String answers = (String)inputStream.readObject();
                    System.out.println(questions);
                    
                    if(i == 0){
                        PanelLevel1.setVisible(true);
                        PanelLevel2.setVisible(false);
                        PanelLevel3.setVisible(false);
                        PanelLevel4.setVisible(false);
                        PanelLevel5.setVisible(false);
                        
                        question1.setText(questions);
                    }else if(i == 1){
                        
                        PanelLevel1.setVisible(false);
                        PanelLevel2.setVisible(true);
                        PanelLevel3.setVisible(false);
                        PanelLevel4.setVisible(false);
                        PanelLevel5.setVisible(false);
                        question2.setText(questions);
                    }else if(i == 2){
                        PanelLevel1.setVisible(false);
                        PanelLevel2.setVisible(false);
                        PanelLevel3.setVisible(true);
                        PanelLevel4.setVisible(false);
                        PanelLevel5.setVisible(false);
                        question3.setText(questions);
                    }else if(i == 3){
                        PanelLevel1.setVisible(false);
                        PanelLevel2.setVisible(false);
                        PanelLevel3.setVisible(false);
                        PanelLevel4.setVisible(true);
                        PanelLevel5.setVisible(false);
                        question4.setText(questions);
                    }else{
                        PanelLevel1.setVisible(false);
                        PanelLevel2.setVisible(false);
                        PanelLevel3.setVisible(false);
                        PanelLevel4.setVisible(false);
                        PanelLevel5.setVisible(true);
                        question5.setText(questions);
                    }
                   
//                    answer = input.nextLine();
                      while(mouseInput == false){
                          Thread.sleep(1000);
                      }
                      
//                      System.out.println(answerMouse);
                      answer = answerMouse;
                    if(checkAnswers(answer, answers) == true){
                        Scores += 20;
                    }
                    i++;
                    currentLevel++;
                    mouseInput = false;
                    
                }
                System.out.println("Your Scores :"+Scores);
                isRunning = false;
                new EndGame().setVisible(true);
                dispose();
                
            }
           
       }catch(Exception e){
           e.printStackTrace();
       }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        PanelLevel1 = new javax.swing.JPanel();
        p1current_level = new javax.swing.JLabel();
        p1heart1 = new javax.swing.JLabel();
        p1heart2 = new javax.swing.JLabel();
        p1heart3 = new javax.swing.JLabel();
        question1 = new javax.swing.JLabel();
        p1choiseD = new javax.swing.JLabel();
        p1choiseA = new javax.swing.JLabel();
        p1choiseB = new javax.swing.JLabel();
        p1choiseC = new javax.swing.JLabel();
        PanelLevel2 = new javax.swing.JPanel();
        p2current_level = new javax.swing.JLabel();
        p2heart1 = new javax.swing.JLabel();
        p2heart2 = new javax.swing.JLabel();
        p2heart3 = new javax.swing.JLabel();
        answer2 = new javax.swing.JLabel();
        p2choiseD = new javax.swing.JLabel();
        p2choiseA = new javax.swing.JLabel();
        p2choiseB = new javax.swing.JLabel();
        p2choiseC = new javax.swing.JLabel();
        question2 = new javax.swing.JLabel();
        PanelLevel3 = new javax.swing.JPanel();
        p3current_level = new javax.swing.JLabel();
        p3heart1 = new javax.swing.JLabel();
        p3heart2 = new javax.swing.JLabel();
        p3heart3 = new javax.swing.JLabel();
        question3 = new javax.swing.JLabel();
        p3choiseD = new javax.swing.JLabel();
        p3choiseA = new javax.swing.JLabel();
        p3choiseB = new javax.swing.JLabel();
        p3choiseC = new javax.swing.JLabel();
        PanelLevel4 = new javax.swing.JPanel();
        p4current_level = new javax.swing.JLabel();
        p4heart1 = new javax.swing.JLabel();
        p4heart2 = new javax.swing.JLabel();
        p4heart3 = new javax.swing.JLabel();
        question4 = new javax.swing.JLabel();
        p4choiseD = new javax.swing.JLabel();
        p4choiseA = new javax.swing.JLabel();
        p4choiseB = new javax.swing.JLabel();
        p4choiseC = new javax.swing.JLabel();
        PanelLevel5 = new javax.swing.JPanel();
        p5current_level = new javax.swing.JLabel();
        p5heart1 = new javax.swing.JLabel();
        p5heart2 = new javax.swing.JLabel();
        p5heart3 = new javax.swing.JLabel();
        answer5 = new javax.swing.JLabel();
        p5choiseD = new javax.swing.JLabel();
        p5choiseA = new javax.swing.JLabel();
        p5choiseB = new javax.swing.JLabel();
        p5choiseC = new javax.swing.JLabel();
        question5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(641, 475));

        PanelLevel1.setBackground(new java.awt.Color(44, 44, 44));
        PanelLevel1.setMaximumSize(new java.awt.Dimension(641, 475));
        PanelLevel1.setMinimumSize(new java.awt.Dimension(641, 475));
        PanelLevel1.setPreferredSize(new java.awt.Dimension(641, 475));
        PanelLevel1.setLayout(null);

        p1current_level.setBackground(new java.awt.Color(200, 29, 1));
        p1current_level.setFont(new java.awt.Font("TlwgTypewriter", 1, 48)); // NOI18N
        p1current_level.setForeground(new java.awt.Color(225, 1, 1));
        p1current_level.setText("#1");
        PanelLevel1.add(p1current_level);
        p1current_level.setBounds(20, 20, 60, 40);

        p1heart1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel1.add(p1heart1);
        p1heart1.setBounds(560, 20, 40, 39);

        p1heart2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel1.add(p1heart2);
        p1heart2.setBounds(510, 20, 40, 39);

        p1heart3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel1.add(p1heart3);
        p1heart3.setBounds(460, 20, 40, 39);

        question1.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        question1.setForeground(new java.awt.Color(255, 231, 0));
        question1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        question1.setText("Question 1");
        question1.setMaximumSize(new java.awt.Dimension(590, 170));
        question1.setMinimumSize(new java.awt.Dimension(590, 170));
        question1.setPreferredSize(new java.awt.Dimension(590, 170));
        PanelLevel1.add(question1);
        question1.setBounds(70, 70, 510, 170);

        p1choiseD.setBackground(new java.awt.Color(108, 108, 108));
        p1choiseD.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p1choiseD.setForeground(new java.awt.Color(254, 254, 254));
        p1choiseD.setText("  D. 01");
        p1choiseD.setOpaque(true);
        p1choiseD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p1choiseDMouseClicked(evt);
            }
        });
        PanelLevel1.add(p1choiseD);
        p1choiseD.setBounds(360, 340, 200, 70);

        p1choiseA.setBackground(new java.awt.Color(108, 108, 108));
        p1choiseA.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p1choiseA.setForeground(new java.awt.Color(254, 254, 254));
        p1choiseA.setText("  A. Satu");
        p1choiseA.setOpaque(true);
        p1choiseA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p1choiseAMouseClicked(evt);
            }
        });
        PanelLevel1.add(p1choiseA);
        p1choiseA.setBounds(70, 240, 200, 70);

        p1choiseB.setBackground(new java.awt.Color(108, 108, 108));
        p1choiseB.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p1choiseB.setForeground(new java.awt.Color(254, 254, 254));
        p1choiseB.setText("  B. 1");
        p1choiseB.setOpaque(true);
        p1choiseB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p1choiseBMouseClicked(evt);
            }
        });
        PanelLevel1.add(p1choiseB);
        p1choiseB.setBounds(70, 340, 200, 70);

        p1choiseC.setBackground(new java.awt.Color(108, 108, 108));
        p1choiseC.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p1choiseC.setForeground(new java.awt.Color(254, 254, 254));
        p1choiseC.setText("  C. One");
        p1choiseC.setOpaque(true);
        p1choiseC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p1choiseCMouseClicked(evt);
            }
        });
        PanelLevel1.add(p1choiseC);
        p1choiseC.setBounds(360, 240, 200, 70);

        PanelLevel2.setBackground(new java.awt.Color(54, 132, 0));
        PanelLevel2.setMaximumSize(new java.awt.Dimension(641, 475));
        PanelLevel2.setMinimumSize(new java.awt.Dimension(641, 475));
        PanelLevel2.setPreferredSize(new java.awt.Dimension(641, 475));
        PanelLevel2.setLayout(null);

        p2current_level.setBackground(new java.awt.Color(200, 29, 1));
        p2current_level.setFont(new java.awt.Font("TlwgTypewriter", 1, 48)); // NOI18N
        p2current_level.setForeground(new java.awt.Color(225, 1, 1));
        p2current_level.setText("#2");
        PanelLevel2.add(p2current_level);
        p2current_level.setBounds(20, 20, 60, 40);

        p2heart1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel2.add(p2heart1);
        p2heart1.setBounds(560, 20, 40, 39);

        p2heart2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel2.add(p2heart2);
        p2heart2.setBounds(510, 20, 40, 39);

        p2heart3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel2.add(p2heart3);
        p2heart3.setBounds(460, 20, 40, 39);

        answer2.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        answer2.setForeground(new java.awt.Color(255, 231, 0));
        answer2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        answer2.setText("1");
        answer2.setToolTipText("");
        answer2.setMaximumSize(new java.awt.Dimension(590, 170));
        answer2.setMinimumSize(new java.awt.Dimension(590, 170));
        answer2.setPreferredSize(new java.awt.Dimension(590, 170));
        answer2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                answer2MouseClicked(evt);
            }
        });
        PanelLevel2.add(answer2);
        answer2.setBounds(90, 140, 20, 30);

        p2choiseD.setBackground(new java.awt.Color(108, 108, 108));
        p2choiseD.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p2choiseD.setForeground(new java.awt.Color(254, 254, 254));
        p2choiseD.setText("  D. 623");
        p2choiseD.setOpaque(true);
        p2choiseD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p2choiseDMouseClicked(evt);
            }
        });
        PanelLevel2.add(p2choiseD);
        p2choiseD.setBounds(360, 340, 200, 70);

        p2choiseA.setBackground(new java.awt.Color(108, 108, 108));
        p2choiseA.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p2choiseA.setForeground(new java.awt.Color(254, 254, 254));
        p2choiseA.setText("  A. 256");
        p2choiseA.setOpaque(true);
        p2choiseA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p2choiseAMouseClicked(evt);
            }
        });
        PanelLevel2.add(p2choiseA);
        p2choiseA.setBounds(70, 240, 200, 70);

        p2choiseB.setBackground(new java.awt.Color(108, 108, 108));
        p2choiseB.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p2choiseB.setForeground(new java.awt.Color(254, 254, 254));
        p2choiseB.setText("  B. 265");
        p2choiseB.setOpaque(true);
        p2choiseB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p2choiseBMouseClicked(evt);
            }
        });
        PanelLevel2.add(p2choiseB);
        p2choiseB.setBounds(70, 340, 200, 70);

        p2choiseC.setBackground(new java.awt.Color(108, 108, 108));
        p2choiseC.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p2choiseC.setForeground(new java.awt.Color(254, 254, 254));
        p2choiseC.setText("  C. 526");
        p2choiseC.setOpaque(true);
        p2choiseC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p2choiseCMouseClicked(evt);
            }
        });
        PanelLevel2.add(p2choiseC);
        p2choiseC.setBounds(360, 240, 200, 70);

        question2.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        question2.setForeground(new java.awt.Color(255, 231, 0));
        question2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        question2.setText("Question 2");
        question2.setToolTipText("");
        question2.setMaximumSize(new java.awt.Dimension(590, 170));
        question2.setMinimumSize(new java.awt.Dimension(590, 170));
        question2.setPreferredSize(new java.awt.Dimension(590, 170));
        PanelLevel2.add(question2);
        question2.setBounds(110, 70, 450, 170);

        PanelLevel3.setBackground(new java.awt.Color(39, 45, 77));
        PanelLevel3.setMaximumSize(new java.awt.Dimension(641, 475));
        PanelLevel3.setMinimumSize(new java.awt.Dimension(641, 475));
        PanelLevel3.setPreferredSize(new java.awt.Dimension(641, 475));
        PanelLevel3.setLayout(null);

        p3current_level.setBackground(new java.awt.Color(200, 29, 1));
        p3current_level.setFont(new java.awt.Font("TlwgTypewriter", 1, 48)); // NOI18N
        p3current_level.setForeground(new java.awt.Color(225, 1, 1));
        p3current_level.setText("#3");
        PanelLevel3.add(p3current_level);
        p3current_level.setBounds(20, 20, 60, 40);

        p3heart1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel3.add(p3heart1);
        p3heart1.setBounds(560, 20, 40, 39);

        p3heart2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel3.add(p3heart2);
        p3heart2.setBounds(510, 20, 40, 39);

        p3heart3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel3.add(p3heart3);
        p3heart3.setBounds(460, 20, 40, 39);

        question3.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        question3.setForeground(new java.awt.Color(255, 231, 0));
        question3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        question3.setText("Question 3");
        question3.setMaximumSize(new java.awt.Dimension(590, 170));
        question3.setMinimumSize(new java.awt.Dimension(590, 170));
        question3.setPreferredSize(new java.awt.Dimension(590, 170));
        PanelLevel3.add(question3);
        question3.setBounds(70, 70, 510, 170);

        p3choiseD.setBackground(new java.awt.Color(108, 108, 108));
        p3choiseD.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p3choiseD.setForeground(new java.awt.Color(254, 254, 254));
        p3choiseD.setText("  D. Aku");
        p3choiseD.setOpaque(true);
        p3choiseD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p3choiseDMouseClicked(evt);
            }
        });
        PanelLevel3.add(p3choiseD);
        p3choiseD.setBounds(360, 340, 200, 70);

        p3choiseA.setBackground(new java.awt.Color(108, 108, 108));
        p3choiseA.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p3choiseA.setForeground(new java.awt.Color(254, 254, 254));
        p3choiseA.setText("  A. Malam");
        p3choiseA.setOpaque(true);
        p3choiseA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p3choiseAMouseClicked(evt);
            }
        });
        PanelLevel3.add(p3choiseA);
        p3choiseA.setBounds(70, 240, 200, 70);

        p3choiseB.setBackground(new java.awt.Color(108, 108, 108));
        p3choiseB.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p3choiseB.setForeground(new java.awt.Color(254, 254, 254));
        p3choiseB.setText("  B. Api");
        p3choiseB.setOpaque(true);
        p3choiseB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p3choiseBMouseClicked(evt);
            }
        });
        PanelLevel3.add(p3choiseB);
        p3choiseB.setBounds(70, 340, 200, 70);

        p3choiseC.setBackground(new java.awt.Color(108, 108, 108));
        p3choiseC.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p3choiseC.setForeground(new java.awt.Color(254, 254, 254));
        p3choiseC.setText("  C. Kamu");
        p3choiseC.setOpaque(true);
        p3choiseC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p3choiseCMouseClicked(evt);
            }
        });
        PanelLevel3.add(p3choiseC);
        p3choiseC.setBounds(360, 240, 200, 70);

        PanelLevel4.setBackground(new java.awt.Color(39, 45, 77));
        PanelLevel4.setMaximumSize(new java.awt.Dimension(641, 475));
        PanelLevel4.setMinimumSize(new java.awt.Dimension(641, 475));
        PanelLevel4.setPreferredSize(new java.awt.Dimension(641, 475));
        PanelLevel4.setLayout(null);

        p4current_level.setBackground(new java.awt.Color(200, 29, 1));
        p4current_level.setFont(new java.awt.Font("TlwgTypewriter", 1, 48)); // NOI18N
        p4current_level.setForeground(new java.awt.Color(225, 1, 1));
        p4current_level.setText("#4");
        PanelLevel4.add(p4current_level);
        p4current_level.setBounds(20, 20, 60, 40);

        p4heart1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel4.add(p4heart1);
        p4heart1.setBounds(560, 20, 40, 39);

        p4heart2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel4.add(p4heart2);
        p4heart2.setBounds(510, 20, 40, 39);

        p4heart3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel4.add(p4heart3);
        p4heart3.setBounds(460, 20, 40, 39);

        question4.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        question4.setForeground(new java.awt.Color(255, 231, 0));
        question4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        question4.setText("Question 4");
        question4.setMaximumSize(new java.awt.Dimension(590, 170));
        question4.setMinimumSize(new java.awt.Dimension(590, 170));
        question4.setPreferredSize(new java.awt.Dimension(590, 170));
        PanelLevel4.add(question4);
        question4.setBounds(70, 70, 510, 170);

        p4choiseD.setBackground(new java.awt.Color(108, 108, 108));
        p4choiseD.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p4choiseD.setForeground(new java.awt.Color(254, 254, 254));
        p4choiseD.setText("  D. 1001");
        p4choiseD.setOpaque(true);
        p4choiseD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p4choiseDMouseClicked(evt);
            }
        });
        PanelLevel4.add(p4choiseD);
        p4choiseD.setBounds(360, 340, 200, 70);

        p4choiseA.setBackground(new java.awt.Color(108, 108, 108));
        p4choiseA.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p4choiseA.setForeground(new java.awt.Color(254, 254, 254));
        p4choiseA.setText("  A. 0101");
        p4choiseA.setOpaque(true);
        p4choiseA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p4choiseAMouseClicked(evt);
            }
        });
        PanelLevel4.add(p4choiseA);
        p4choiseA.setBounds(70, 240, 200, 70);

        p4choiseB.setBackground(new java.awt.Color(108, 108, 108));
        p4choiseB.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p4choiseB.setForeground(new java.awt.Color(254, 254, 254));
        p4choiseB.setText("  B. 0100");
        p4choiseB.setOpaque(true);
        p4choiseB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p4choiseBMouseClicked(evt);
            }
        });
        PanelLevel4.add(p4choiseB);
        p4choiseB.setBounds(70, 340, 200, 70);

        p4choiseC.setBackground(new java.awt.Color(108, 108, 108));
        p4choiseC.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p4choiseC.setForeground(new java.awt.Color(254, 254, 254));
        p4choiseC.setText("  C. 1011");
        p4choiseC.setOpaque(true);
        p4choiseC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p4choiseCMouseClicked(evt);
            }
        });
        PanelLevel4.add(p4choiseC);
        p4choiseC.setBounds(360, 240, 200, 70);

        PanelLevel5.setBackground(new java.awt.Color(39, 45, 77));
        PanelLevel5.setMinimumSize(new java.awt.Dimension(641, 475));
        PanelLevel5.setPreferredSize(new java.awt.Dimension(641, 475));
        PanelLevel5.setLayout(null);

        p5current_level.setBackground(new java.awt.Color(200, 29, 1));
        p5current_level.setFont(new java.awt.Font("TlwgTypewriter", 1, 48)); // NOI18N
        p5current_level.setForeground(new java.awt.Color(225, 1, 1));
        p5current_level.setText("#5");
        PanelLevel5.add(p5current_level);
        p5current_level.setBounds(20, 20, 60, 40);

        p5heart1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel5.add(p5heart1);
        p5heart1.setBounds(560, 20, 40, 39);

        p5heart2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel5.add(p5heart2);
        p5heart2.setBounds(510, 20, 40, 39);

        p5heart3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/heart.png"))); // NOI18N
        PanelLevel5.add(p5heart3);
        p5heart3.setBounds(460, 20, 40, 39);

        answer5.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        answer5.setForeground(new java.awt.Color(255, 231, 0));
        answer5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        answer5.setText("Panca");
        answer5.setMaximumSize(new java.awt.Dimension(590, 170));
        answer5.setMinimumSize(new java.awt.Dimension(590, 170));
        answer5.setPreferredSize(new java.awt.Dimension(590, 170));
        answer5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                answer5MouseClicked(evt);
            }
        });
        PanelLevel5.add(answer5);
        answer5.setBounds(210, 130, 120, 50);

        p5choiseD.setBackground(new java.awt.Color(108, 108, 108));
        p5choiseD.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p5choiseD.setForeground(new java.awt.Color(254, 254, 254));
        p5choiseD.setText(" D. Sila ke 4");
        p5choiseD.setOpaque(true);
        p5choiseD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p5choiseDMouseClicked(evt);
            }
        });
        PanelLevel5.add(p5choiseD);
        p5choiseD.setBounds(360, 340, 220, 70);

        p5choiseA.setBackground(new java.awt.Color(108, 108, 108));
        p5choiseA.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p5choiseA.setForeground(new java.awt.Color(254, 254, 254));
        p5choiseA.setText(" A. Sila ke 1");
        p5choiseA.setOpaque(true);
        p5choiseA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p5choiseAMouseClicked(evt);
            }
        });
        PanelLevel5.add(p5choiseA);
        p5choiseA.setBounds(70, 240, 220, 70);

        p5choiseB.setBackground(new java.awt.Color(108, 108, 108));
        p5choiseB.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p5choiseB.setForeground(new java.awt.Color(254, 254, 254));
        p5choiseB.setText(" B. Sila ke 2");
        p5choiseB.setOpaque(true);
        p5choiseB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p5choiseBMouseClicked(evt);
            }
        });
        PanelLevel5.add(p5choiseB);
        p5choiseB.setBounds(70, 340, 220, 70);

        p5choiseC.setBackground(new java.awt.Color(108, 108, 108));
        p5choiseC.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        p5choiseC.setForeground(new java.awt.Color(254, 254, 254));
        p5choiseC.setText(" C. Sila ke 3");
        p5choiseC.setOpaque(true);
        p5choiseC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                p5choiseCMouseClicked(evt);
            }
        });
        PanelLevel5.add(p5choiseC);
        p5choiseC.setBounds(360, 240, 220, 70);

        question5.setFont(new java.awt.Font("Chilanka", 1, 36)); // NOI18N
        question5.setForeground(new java.awt.Color(255, 231, 0));
        question5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        question5.setText("Question 5");
        question5.setMaximumSize(new java.awt.Dimension(590, 170));
        question5.setMinimumSize(new java.awt.Dimension(590, 170));
        question5.setPreferredSize(new java.awt.Dimension(590, 170));
        PanelLevel5.add(question5);
        question5.setBounds(330, 70, 230, 170);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PanelLevel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(PanelLevel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(PanelLevel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(PanelLevel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(PanelLevel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(PanelLevel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PanelLevel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PanelLevel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PanelLevel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(PanelLevel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void p1choiseAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p1choiseAMouseClicked
       
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p1heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
       }else{
           if(p1heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
       
    }//GEN-LAST:event_p1choiseAMouseClicked

    private void p1choiseBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p1choiseBMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p1heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
       }else{
           if(p1heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p1choiseBMouseClicked

    private void p1choiseCMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p1choiseCMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p1heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
       }else{
           if(p1heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p1choiseCMouseClicked

    private void p1choiseDMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p1choiseDMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p1heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           
       }else{
           if(p1heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p1choiseDMouseClicked

    private void answer2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_answer2MouseClicked
        mouseInput = true;
        this.answerMouse = "1";
    }//GEN-LAST:event_answer2MouseClicked

    private void p2choiseAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p2choiseAMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p2heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p2heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;        
    }//GEN-LAST:event_p2choiseAMouseClicked

    private void p2choiseCMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p2choiseCMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p2heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p2heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p2choiseCMouseClicked

    private void p2choiseBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p2choiseBMouseClicked
        if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p2heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p2heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p2choiseBMouseClicked

    private void p2choiseDMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p2choiseDMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p2heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p2heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p2choiseDMouseClicked

    private void p3choiseAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p3choiseAMouseClicked
        if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p3heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p3heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1; 
    }//GEN-LAST:event_p3choiseAMouseClicked

    private void p3choiseBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p3choiseBMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p3heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p3heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1; 
    }//GEN-LAST:event_p3choiseBMouseClicked

    private void p3choiseDMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p3choiseDMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p3heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p3heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p3choiseDMouseClicked

    private void p3choiseCMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p3choiseCMouseClicked
        mouseInput = true;
        this.answerMouse = "kamu";
    }//GEN-LAST:event_p3choiseCMouseClicked

    private void p4choiseBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p4choiseBMouseClicked
        mouseInput = true;
        this.answerMouse = "0100";
    }//GEN-LAST:event_p4choiseBMouseClicked

    private void p4choiseAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p4choiseAMouseClicked
        if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p4heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p4heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p4choiseAMouseClicked

    private void p4choiseCMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p4choiseCMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p4heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p4heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p4choiseCMouseClicked

    private void p4choiseDMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p4choiseDMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p4heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p4heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p4choiseDMouseClicked

    private void p5choiseAMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p5choiseAMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p5heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p5heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p5choiseAMouseClicked

    private void p5choiseCMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p5choiseCMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p5heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p5heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p5choiseCMouseClicked

    private void p5choiseBMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p5choiseBMouseClicked
      if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p5heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p5heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p5choiseBMouseClicked

    private void p5choiseDMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_p5choiseDMouseClicked
       if(heart == 1){
           new Home().setVisible(true);
           this.dispose();
       }
       if(p5heart1.isVisible()){
           p1heart1.setVisible(false);
           p2heart1.setVisible(false);
           p3heart1.setVisible(false);
           p4heart1.setVisible(false);
           p5heart1.setVisible(false);
           
       }else{
           if(p5heart2.isVisible()){
                p1heart2.setVisible(false);
                p2heart2.setVisible(false);
                p3heart2.setVisible(false);
                p4heart2.setVisible(false);
                p5heart2.setVisible(false);
           }else{
                p1heart3.setVisible(false);
                p2heart3.setVisible(false);
                p3heart3.setVisible(false);
                p4heart3.setVisible(false);
                p5heart3.setVisible(false);
           }
       }
       heart -= 1;
    }//GEN-LAST:event_p5choiseDMouseClicked

    private void answer5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_answer5MouseClicked
        mouseInput = true;
        this.answerMouse = "panca";
    }//GEN-LAST:event_answer5MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        GamePlay gp = new GamePlay();
        gp.setVisible(true);
        new Thread(gp).start();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PanelLevel1;
    private javax.swing.JPanel PanelLevel2;
    private javax.swing.JPanel PanelLevel3;
    private javax.swing.JPanel PanelLevel4;
    private javax.swing.JPanel PanelLevel5;
    private javax.swing.JLabel answer2;
    private javax.swing.JLabel answer5;
    private javax.swing.JLabel p1choiseA;
    private javax.swing.JLabel p1choiseB;
    private javax.swing.JLabel p1choiseC;
    private javax.swing.JLabel p1choiseD;
    private javax.swing.JLabel p1current_level;
    private javax.swing.JLabel p1heart1;
    private javax.swing.JLabel p1heart2;
    private javax.swing.JLabel p1heart3;
    private javax.swing.JLabel p2choiseA;
    private javax.swing.JLabel p2choiseB;
    private javax.swing.JLabel p2choiseC;
    private javax.swing.JLabel p2choiseD;
    private javax.swing.JLabel p2current_level;
    private javax.swing.JLabel p2heart1;
    private javax.swing.JLabel p2heart2;
    private javax.swing.JLabel p2heart3;
    private javax.swing.JLabel p3choiseA;
    private javax.swing.JLabel p3choiseB;
    private javax.swing.JLabel p3choiseC;
    private javax.swing.JLabel p3choiseD;
    private javax.swing.JLabel p3current_level;
    private javax.swing.JLabel p3heart1;
    private javax.swing.JLabel p3heart2;
    private javax.swing.JLabel p3heart3;
    private javax.swing.JLabel p4choiseA;
    private javax.swing.JLabel p4choiseB;
    private javax.swing.JLabel p4choiseC;
    private javax.swing.JLabel p4choiseD;
    private javax.swing.JLabel p4current_level;
    private javax.swing.JLabel p4heart1;
    private javax.swing.JLabel p4heart2;
    private javax.swing.JLabel p4heart3;
    private javax.swing.JLabel p5choiseA;
    private javax.swing.JLabel p5choiseB;
    private javax.swing.JLabel p5choiseC;
    private javax.swing.JLabel p5choiseD;
    private javax.swing.JLabel p5current_level;
    private javax.swing.JLabel p5heart1;
    private javax.swing.JLabel p5heart2;
    private javax.swing.JLabel p5heart3;
    private javax.swing.JLabel question1;
    private javax.swing.JLabel question2;
    private javax.swing.JLabel question3;
    private javax.swing.JLabel question4;
    private javax.swing.JLabel question5;
    // End of variables declaration//GEN-END:variables

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(key == KeyEvent.VK_1){
            if(currentLevel == 0){
                mouseInput = true;
                this.answerMouse = "1";
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }
}
