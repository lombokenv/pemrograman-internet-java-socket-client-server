/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quizfinal;

public class Quiz {
    private int number;
    private String questions;
    private String answers;
    
    public Quiz(int number, String questions, String answers){
        this.number = number;
        this.questions = questions;
        this.answers = answers;
    }
    public int getNumberQuiz(){
        return this.number;
    }
    public String getQestionQuiz(){
        return this.questions;
    }
    public String getAnswerQuiz(){
        return this.answers;
    }
    
}
